object FileExplorerProgram extends App {
  for (file <- new FileExplorer(args(0)).filesInDirectory if file.getName.startsWith("."))
    println(file)
}
