class Fraction(n: Int, d: Int) {

  require(d != 0)

  private val g = gcd(n abs, d abs)
  val numerator: Int = n / g
  val denominator: Int = d / g

  def this(n: Int) = this(n, 1)

  override def toString = s"$numerator/$denominator"

  def +(that: Fraction): Fraction = new Fraction(numerator * that.denominator + denominator * that.numerator,
    denominator * that.denominator)

  def +(operand: Int): Fraction = new Fraction(numerator + denominator * operand, denominator)

  def *(that: Fraction): Fraction = new Fraction(numerator * that.numerator, denominator * that.denominator)

  def *(operand: Int): Fraction = new Fraction(numerator * operand, denominator)

  def -(that: Fraction): Fraction = new Fraction(numerator * that.denominator - denominator * that.numerator,
    denominator * that.denominator)

  def -(sub: Int): Fraction = new Fraction(numerator - denominator * sub, denominator)

  def /(divisor: Fraction): Fraction = new Fraction(numerator * divisor.denominator, denominator * divisor.numerator)

  def /(divisor: Int): Fraction = new Fraction(numerator, denominator * divisor)

  def <(that: Fraction): Boolean = numerator * that.denominator < that.numerator * denominator

  def max(that: Fraction): Fraction = if(this < that) that else this

  private def gcd(a: Int, b: Int): Int = if(b == 0) a else gcd(b, a % b)
}