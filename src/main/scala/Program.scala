object Program extends App {
  val fraction1 = new Fraction(args(0) toInt, args(1) toInt)
  val fraction2 = new Fraction(args(2) toInt, args(3) toInt)
  println(s"$fraction1 + $fraction2 = ${fraction1 + fraction2}")
}
