import java.io.File

class FileExplorer(dir: String) {
  require(dir != null && dir.trim.length > 0)

  val directory: String = dir trim

  def filesInDirectory: Array[File] = new File(directory).listFiles()
}
